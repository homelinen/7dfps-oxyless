package Oxyfps;

/**
 * Simple Item class, for tracking quantity of an item
 * 
 * @author homelinen
 * 
 */
public class Item {
	private String name;
	private int quantity;

	public Item(String name, int quantity) {
		this.name = name;
		this.quantity = quantity;
	}

	public Item(String name) {
		this.name = name;
		this.quantity = 1;
	}

	public void incQuantity() {
		quantity++;
	}

	public void decQuantity() {
		// No negative items
		if (--quantity < 0)
			return;
		quantity--;
	}

	public String getName() {
		return name;
	}

	public int getQuantity() {
		return quantity;
	}
}
