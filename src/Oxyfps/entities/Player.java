package Oxyfps.entities;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import Oxyfps.InputController;
import Oxyfps.UI.PlayerUI;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.objects.PhysicsCharacter;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;

public class Player extends Unit {

	// Inventory could just be a list of objects
	private List<Item> inventory;

	private CharacterControl physControl;

	// Players contain their own UI
	PlayerUI hud;

	public Player(float height, float health, float oxygen,
			AppSettings settings, AssetManager mgr, Node gui) {
		super(height, health, oxygen);
		this.inventory = new LinkedList<Item>();
		this.hud = new PlayerUI(this, settings, mgr, gui);

		setupPlayer();
	}

	/**
	 * 
	 * TODO: This should maybe be generic
	 */
	public void setupPlayer() {

		// Size of player in meters, as a capsule
		CapsuleCollisionShape capShape = new CapsuleCollisionShape(0.3f, 2f, 1);

		physControl = new CharacterControl(capShape, 0.3f);
		// Player phys settings here
		physControl.setJumpSpeed(20);
		physControl.setFallSpeed(30);
		physControl.setGravity(30);

		// Inventory Test
		addItem(new Item("Arrows", 50));
		addItem(new Item("Knifes", 3));

	}

	public void update() {

		calcOxyConsumeRate();
		// TODO: This should be per second
		decrementOxygen();
		hud.update();

		updateSpeed();
	}

	/**
	 * Handles the player movement Returns where the player has moved to
	 * 
	 * @param inputControl
	 * @param forwardVed
	 *            The vector representing movement on Z - respective to player
	 * @param leftVec
	 *            The vector representing movement on x - respective to player
	 * @return
	 */
	public Vector3f controlHandle(InputController inputControl,
			Vector3f forwardVec, Vector3f leftVec) {

		// TODO: Don't change if you don't need to
		this.setSprinting(inputControl.isSprintPressed());

		forwardVec.multLocal(this.getMoveSpeed());
		leftVec.multLocal(this.getSideStepSpeed());

		Vector3f walkDirection = new Vector3f(0, 0, 0);

		if (inputControl.isLeft()) {
			walkDirection.addLocal(leftVec);
		}
		if (inputControl.isRight()) {
			walkDirection.addLocal(leftVec.negate());
		}
		if (inputControl.isUp()) {
			walkDirection.addLocal(forwardVec);
		}
		if (inputControl.isDown()) {
			walkDirection.addLocal(forwardVec.negate());
		}

		if (inputControl.hasJump()) {
			this.getPhysControl().jump();
		}

		// This is a debug thing
		if (inputControl.canFly()) {
			this.enableFly();
		} else {
			this.disableFly();
		}

		physControl.setWalkDirection(walkDirection);

		Vector3f newPosition = getPhysicsLocation().add(0, getHeight(), 0);

		return newPosition;
	}

	public void setWalkDirection(Vector3f v) {
		physControl.setWalkDirection(v);
	}

    
    /**
     * Special control Handle for things needing rootNode
     * 
     * @param inputControl
     * @param rootNode
     */
    public void controlHandle(InputController inputControl, Node rootNode, Vector3f direction) {
		
		if (inputControl.isUsing()) {
			searchObject(rootNode, direction);
		}
    }
    
    /**
     * Use a raycast to find a useable object
     */
    private void searchObject(Node rootNode, Vector3f direction) {
    	Vector3f rayStart = physControl.getPhysicsLocation();
    	
    	//Create a ray where player is looking
    	Ray ray = new Ray(rayStart, direction);
    	CollisionResults results = new CollisionResults();
    	
    	//Check if ray collides with anything
    	rootNode.collideWith(ray, results);
    	    
    	float aproxArmLength = 10;
    	
    	//Check there are results, and they're not too far away
    	if (results.size() > 0 && results.getClosestCollision().getDistance() < aproxArmLength) {
    		
    		Geometry picked = results.getClosestCollision().getGeometry();
    	
    		Object useableData = picked.getUserData("usable");
    		
    		//Get the closest object and see if it's usable
			if (useableData != null && useableData.equals(true)) {
				//Use the object
				Logger.getLogger(Player.class.getName()).log(Level.INFO, "Selected {0}", picked.getName());
				
				//Should generically just use use on a useableType in user data
				if (picked.getUserData("aircapacity") != null) {
					
					//Create an AirTank helper
					AirTank airtank = new AirTank((Float) picked.getUserData("aircapacity"));
					
					airtank.use(this);
										
					//Put the changed value back
					picked.setUserData("aircapacity", airtank.getOxyLevel());
				} else if(picked.getName().equals("Base")) {
					((PlayerBase) picked).use(this);
					Logger.getLogger(Player.class.getName()).log(Level.INFO, "Base has {0}l air", ((PlayerBase) picked).getOxyLevel());
				}
			} else {
				Logger.getLogger(Player.class.getName()).log(Level.INFO, "Selected Nothing Selectable because user data is: {0}" +
						"\nAnd you are looking at {1}", 
						new Object[] { picked, physControl.getWalkDirection() });
			}
    	}
	}

	public void enableFly() {
     	physControl.setJumpSpeed(20);
        physControl.setFallSpeed(0);
        physControl.setGravity(0);
    }
    
    public void disableFly() {
    	physControl.setJumpSpeed(20);
        physControl.setFallSpeed(30);
        physControl.setGravity(30);
    }

	public void addLocalDirection(Vector3f direction) {

	}

	public void addItem(Item item) {
		inventory.add(item);
	}

	public PhysicsCharacter getPhysControl() {
		return physControl;
	}

	public Vector3f getPhysicsLocation() {
		return physControl.getPhysicsLocation();
	}

	public List<Item> getInventory() {
		return inventory;
	}
}
