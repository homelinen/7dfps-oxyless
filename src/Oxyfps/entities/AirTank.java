package Oxyfps.entities;

import com.jme3.scene.Geometry;

public class AirTank implements Usable {
	private float oxygenLevel;
	

	/**
	 * Create an OxyGen tank
	 * 
	 * @param tank Geometry representing the tank
	 */
	public AirTank(float oxygenLevel) {
		this.oxygenLevel = oxygenLevel;
	}
	
	public boolean use(Unit unit) {
		if (oxygenLevel > 0) {
			oxygenLevel--;
			unit.incrementOxygen();
			return true;
		}
		
		return false;
	}
	
	public float getOxyLevel() {
		return oxygenLevel;
	}
}
