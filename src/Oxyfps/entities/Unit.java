package Oxyfps.entities;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.scene.Spatial;

public abstract class Unit implements PhysicsCollisionListener {
	private float height;
	private float moveSpeed;
	private float sideStepSpeed;
	private String name;

	private boolean isSprinting;

	private float health;
	private float oxygen;
	private float oxyConsumeRate;

	private boolean alive;

	public Unit(float height, float health, float oxygen) {
		this.height = height;
		this.health = health;
		this.oxygen = oxygen;
		this.oxyConsumeRate = 0.01f;

		this.moveSpeed = 0.6f;
		this.sideStepSpeed = 0.4f;

		this.isSprinting = false;
		this.alive = true;
		
		this.name = "Unit";
	}
	
	public Unit(String name, float height, float health, float oxygen) {
		this.height = height;
		this.health = health;
		this.oxygen = oxygen;
		this.oxyConsumeRate = 0.01f;

		this.moveSpeed = 0.6f;
		this.sideStepSpeed = 0.4f;

		this.isSprinting = false;
		this.alive = true;
		
		this.name = name;
	}

	public void updateSpeed() {

		float sprintConst = 1.5f;

		// TODO: Magic Number
		moveSpeed = 0.6f;

		if (isSprinting) {
			moveSpeed = moveSpeed * sprintConst;
		}
	}

	public void calcOxyConsumeRate() {

		// Multiply movement speed by lung capacity
		oxyConsumeRate = moveSpeed * 0.03f;
	}

	public float getHeight() {
		return height;
	}

	public float getHealth() {
		return this.health;
	}

	public float getOxygen() {
		return this.oxygen;
	}

	public void decrementOxygen() {
		if (oxygen <= 0)
			decrementHealth();
		oxygen -= oxyConsumeRate;
	}

	public void decrementHealth() {
		decreaseHealth(1);
	}

	public void decreaseHealth(float amount) {
		
		if ((health - amount) <= 0)
			this.alive = false;
		else {
			health -= amount;
		}
	}
	
	/**
	 * Handle collisions with units.
	 */
	public void collision(PhysicsCollisionEvent event) {
		
		if (event.getNodeA() != null && event.getNodeB() != null) {
			//Check for bullets colliding with player
			if (event.getNodeA().getName().equals("bullet")
					&& event.getNodeB().getName().equals(getName())) {
				
					takeHit(event.getNodeA());
			} else if (event.getNodeB().getName().equals("bullet")
					&& event.getNodeA().getName().equals(getName())) {
				
					takeHit(event.getNodeB());
			}
			
			Logger.getLogger(Unit.class.getName()).log(Level.INFO, "Colliders are {0} and {1}", 
					new Object[] { event.getNodeA().getName(),event.getNodeB().getName() });
		}
		
	}
	
	private void takeHit(Spatial bullet) {
		if (bullet.getUserData("damage") != null) {
			String holder = bullet.getUserData("damage").toString();
			decreaseHealth(new Float(holder));
		}
		Logger.getLogger(Unit.class.getName()).log(Level.INFO, "{0} Health Reduced to: {1}", 
				new Object[] { getName(), getHealth() });
	}
	
	/**
	 * Increment oxygen by 1
	 * 
	 * BUG: This is possibly too slow
	 */
	public void incrementOxygen() {
		oxygen++;
	}
		
	public float getMoveSpeed() {
		return moveSpeed;
	}

	public void setSprinting(boolean toggle) {
		this.isSprinting = toggle;
	}

	public float getSideStepSpeed() {
		return sideStepSpeed;
	}

	public boolean isAlive() {
		return this.alive;
	}
	
	public String getName() {
		return this.name;
	}

	public abstract void update();

}
