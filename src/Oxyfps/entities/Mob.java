package Oxyfps.entities;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.control.PhysicsControl;
import com.jme3.math.Vector3f;

import Oxyfps.ProjectileManager;
import Oxyfps.WorldTerrain;
import Oxyfps.AI.AIManager;
import Oxyfps.entities.Enemy;

public class Mob {
	
	private Enemy leader;
	private List<Enemy> members;
	private int mobSize;
	private int ID;
	private Vector3f previousHeading;
	
	/**
	 * 
	 * TODO: Mob shouldn't really initialise with Enemies in it, should it?
	 * @param v
	 * @param health
	 * @param size
	 */
	public Mob(Vector3f v, float health, int size){
		ID = AIManager.getInstance().getNextMobID();
		members = new ArrayList<Enemy>();
		leader = new Enemy(v.getZ(), health,100,v,getNextEnemyID(), ID);
		members.add(leader);
		mobSize = size;
		
		int magniture = 20;
		
		for (int i=0; i<size; i++) {
			Vector3f spawnAdjust = new Vector3f(i, i, i).multLocal(
					WorldTerrain.getRandPolar(), 
					10, 
					WorldTerrain.getRandPolar()
					);
			spawnAdjust.multLocal(magniture);
			
			members.add(new Enemy(v.getZ(),health,100,
					v.add(spawnAdjust), getNextEnemyID(), ID));
			
			Logger.getLogger(Mob.class.getName()).log(Level.INFO,
				"Start: {0}", v.add(spawnAdjust));
		}
		
		previousHeading = v;
	}
	
	public void add(Enemy e){
		members.add(e);
	}
	
//	public void kill(int ID){
//		members.set(ID, null);
//	}
	
	public List<Enemy> getAll(){
		return members;
	}
	
	public int getID(){
		return ID;
	}
	
	public int getNextEnemyID() {
		int size = members.size();
		for (int i = 0; i < size - 1; i++) {
			if (members.get(i) == null) {
				return i;
			}
		}
		members.add(null);
		return size + 1;
	}
	
	public boolean isLeader(Enemy e){
		return (leader.getID() == e.getID());
	}
	
	public void update(){
		boolean needsNewHeading = false;
		int enemiesArrived = 0;
		
		if (members.size() < 1) {
			Logger.getLogger(Mob.class.getName()).log(Level.INFO,
					"No members");
		}
		
		for (Enemy e: members){
			if (e != null) {
				
				if (e.getDestination() == null){
					if (isLeader(e)){
						needsNewHeading = true;
						
						Logger.getLogger(Mob.class.getName()).log(Level.INFO,
								"We need a new heading");
					} else {
						e.setDestination(leader.getDestination());
						Logger.getLogger(Mob.class.getName()).log(Level.INFO,
								"Set enemy destination");
					}
				} else if (e.hasArrived()){
					if (!e.getDestination().equals(leader.getDestination())){
						e.setDestination(leader.getDestination());
					} else {
						enemiesArrived++;
					}
					if (enemiesArrived == mobSize){
						needsNewHeading = true;
					}
					Logger.getLogger(Mob.class.getName()).log(Level.INFO,
							"Arrived, get a new heading");
				} else {
					e.update();
				}
			}
		}
		
		if (needsNewHeading){
			
			//leader.setDestination(AIManager.getInstance().findNearest(leader, previousHeading));
			leader.setDestination(AIManager.getInstance().getWaypoint());
			
			enemiesArrived = 0;
			Logger.getLogger(Mob.class.getName()).log(Level.INFO,
					"New heading");
			this.update();
		}
	}
	
	@Override
	public boolean equals(Object other){
		if (other instanceof Mob){
			Mob otherCheck = (Mob) other;
			return (this.ID == otherCheck.ID);
		}
		return false;
	}
}
