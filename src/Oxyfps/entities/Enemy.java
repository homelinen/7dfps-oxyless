/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Oxyfps.entities;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.objects.PhysicsCharacter;
import com.jme3.math.Vector3f;

import Oxyfps.ProjectileManager;
import Oxyfps.WorldTerrain;
import Oxyfps.AI.AIManager;

/**
 * 
 * @author Ryan
 */
public class Enemy extends Unit {
	
	private int ID;
	private int MobID;
	private Vector3f location;
	private Vector3f destination;
	private boolean onJourney;
	private CharacterControl movementController;
	private int arriveDistance = 3;
	
	private boolean shooting;

	public Enemy(float height, float health, float oxygen, Vector3f location, int ID, int MobID) {
		super(height, health, oxygen);
		this.location = location;
		
		onJourney = false;
		this.ID = ID;
		setUpAI();
	}
	
	private void setUpAI(){
		BoxCollisionShape boxShape = new BoxCollisionShape(new Vector3f(2,2,2));
		
		movementController = new CharacterControl(boxShape, 0.3f);
		movementController.setJumpSpeed(20);
		movementController.setFallSpeed(30);
		movementController.setGravity(30);
		movementController.setUseViewDirection(true);
		movementController.setPhysicsLocation(location);
		
		shooting = false;
	}
	
	public void update() {
		if(!this.isAlive()){
			//AIManager.getInstance().kill(MobID, ID);
			Logger.getLogger(Mob.class.getName()).log(Level.INFO,
					"{0} is dead", this.getID());
			return;
		}
		
		movementController.setViewDirection(destination.normalize());
		movementController.setWalkDirection(destination.normalize().divide(5));
		
		Logger.getLogger(Mob.class.getName()).log(Level.INFO,
				"Currently at {0}, heading towards {1}", 
				new Object[] { location, destination.normalize() });
		location = movementController.getPhysicsLocation();
				
	}

	public int getID() {
		return ID;
	}

	public Vector3f getDestination(){
		return destination;
	}

	public boolean isShooting() {
		return this.shooting;
	}
	
	public void setDestination(Vector3f v){
		destination = v;
	}
	
	public Vector3f getLocation(){
		return location;
	}
	
	public CharacterControl getPhysControlEnemy(){
		return movementController;
	}
	
	public boolean hasArrived() {
		return (location.distance(destination)<arriveDistance);
	}
}
