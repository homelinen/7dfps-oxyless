package Oxyfps.entities;

import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;

public class PlayerBase extends Geometry implements Usable {
	
	private float oxygenLevel;
	private float capacity;
	
	public PlayerBase(float capacity) {
		super();
		
		this.capacity = capacity;
		oxygenLevel = capacity;
	}
	
	public PlayerBase(float capacity, String name, Mesh mesh) {
		super(name, mesh);
		
		this.capacity = capacity;
		oxygenLevel = capacity;
	}
	
	public void update() {
		decrementOxygen();
	}

	/**
	 * Take player oxygen and put in base
	 * TODO: This should be optional, first use should show oxy Level
	 */
	public boolean use(Unit unit) {
		if (oxygenLevel < capacity) {
			oxygenLevel++;
			unit.decrementOxygen();
			return true;
		}
		
		return false;
	}
	
	public float getOxyLevel() {
		return oxygenLevel;
	}
	
	/**
	 * Decrease Oxygen level
	 */
	private void decrementOxygen() {
		if (oxygenLevel > 0) {
			oxygenLevel -= 0.2f;
		}
	}
}
