package Oxyfps.entities;

public interface Usable {

	/**
	 * Define what happens when player has selected to use item
	 * 
	 * @param unit Define what used the object
	 * @return Was use succesful
	 */
	public boolean use(Unit unit);
}
