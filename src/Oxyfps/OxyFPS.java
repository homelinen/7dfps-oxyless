package Oxyfps;

import java.util.logging.Level;
import java.util.logging.Logger;

import Oxyfps.AI.AIManager;
import Oxyfps.UI.PopupQueue;
import Oxyfps.Util.ShapeGen;
import Oxyfps.entities.Enemy;
import Oxyfps.entities.Mob;
import Oxyfps.entities.Player;
import Oxyfps.entities.PlayerBase;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.KeyInput;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.UserData;
import com.jme3.system.AppSettings;
import com.jme3.scene.shape.Box;
import com.jme3.system.NanoTimer;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;

import de.lessvoid.nifty.Nifty;

/**
 * test
 */
public class OxyFPS extends SimpleApplication {

	InputController inputControl;

	private float lastShot;

	private BulletAppState bulletAppState;

	private Player player;
	private PlayerBase base;
	
	private WorldTerrain worldTerrain;

	private ProjectileManager projMan;
	private ProjectileGarbageCollector projGarbage;

	private AudioManager audioManager;
	
	private AIManager AIM;
	
	Nifty nifty;
	PopupQueue textDisp;
	
	boolean debug;
	private static final Logger logger = Logger.getLogger(OxyFPS.class
			.getName());

    public static void main(String[] args) {
        OxyFPS app = new OxyFPS();
        app.setSettings(configureSettings());
        app.start();
    }

	private static AppSettings configureSettings() {
		AppSettings settings = new AppSettings(true);
    	settings.setSettingsDialogImage("Interface/splashscreen.png");
		return settings;
	}

    @Override
    public void simpleInitApp() {
    	
    	debug = true;

    	timer = new NanoTimer();
    	lastShot = 0;
    	
    	// Set up physics
    	bulletAppState = new BulletAppState();
    	stateManager.attach(bulletAppState);
    	
    	if (debug) {
    		//Set up logger
    		Logger.getLogger("").setLevel(Level.INFO);
    		bulletAppState.getPhysicsSpace().enableDebug(assetManager);
    	} else {
    		//Set up logger
    		Logger.getLogger("").setLevel(Level.WARNING);
    	}
    	
    	// Set up keys
    	inputControl = new InputController(inputManager, cam);
   	    inputControl.setupKeyBindings(KeyInput.KEY_W, KeyInput.KEY_A, KeyInput.KEY_S, KeyInput.KEY_D, 
   	    		KeyInput.KEY_E, KeyInput.KEY_SPACE, KeyInput.KEY_LSHIFT);	
    	
   	    // Set up Movement speed (I don't know what this is)
    	flyCam.setMoveSpeed(600);

    	flyCam.setEnabled(false);
    	
    	genWorld();
    	
    	AIM = AIManager.getInstance();
    	AIM.setWT(worldTerrain);
    	
    	projGarbage = new ProjectileGarbageCollector(worldTerrain.getWorldName(), rootNode, bulletAppState);
    	bulletAppState.getPhysicsSpace().addCollisionListener(projGarbage);
    	
    	initPlayer();
    	
    	projMan = new ProjectileManager(this, bulletAppState);
    	
    	// Set up AI
    	                
		//GUI testing
		setDisplayStatView(false); 
		setDisplayFps(false);
   
		initAudio();
		
		initBase();
		
		for (int i=0; i < 10; i++) {
			genAirCan();
		}
		initAI();
		initScreenText();
    }

	@Override
	public void simpleUpdate(float tpf) {
		float timeFromLastShot = timer.getTimeInSeconds() - lastShot;
		float bulletBuffer = 0.5f;
		if (inputControl.isFire() && (timeFromLastShot > bulletBuffer)) {
			
			int damage = 2;
			
			//Combine player move speed with constant speed
			float speedFactor = 150f + player.getMoveSpeed();
			
			//Move firing location a few meters in front of player
			Vector3f location = cam.getLocation().add(0, 0, player.getHeight() / 3);
			
			Geometry bullet = ProjectileManager.fireProjectile(assetManager, location, cam.getDirection(), speedFactor, damage);
			
			audioManager.playAudio("bowFire", false);
			
			rootNode.attachChild(bullet);
			bulletAppState.getPhysicsSpace().add(bullet.getControl(0));
			
			logger.log(Level.INFO, "bulletPosition: {0}", bullet.getLocalTranslation());
			
			lastShot = timer.getTimeInSeconds();
		}
		
		Vector3f camPosition = player.controlHandle(inputControl, cam.getDirection(), cam.getLeft());
		cam.setLocation(camPosition);
		
		player.controlHandle(inputControl, rootNode, cam.getDirection());
		player.update();
		
		//This is here due to a bug in JMonkey
    	inputManager.setCursorVisible(false);

    	base.update();
    	
    	textDisp.update(tpf);
    	AIM.update();
    	
    	projMan.update();
	}

    @Override
    public void simpleRender(RenderManager rm) {
    	
    	if (debug) {
    		//Debug Stuff
    		worldTerrain.getMatTerrain().getAdditionalRenderState().setWireframe(inputControl.isWireframe());
    	}
    }
    
    public void genWorld() {
    	
    	Texture skyTex = assetManager.loadTexture("Textures/Sky/sky_base.png");
    	
    	//Gen skybox
    	rootNode.attachChild(SkyFactory.createSky(assetManager, skyTex, skyTex, skyTex, skyTex,skyTex, skyTex));
    	
    	worldTerrain = new WorldTerrain(assetManager);
    	
    	worldTerrain.setupLOD(getCamera());
    	rootNode.attachChild(worldTerrain.getTerrain());

    	bulletAppState.getPhysicsSpace().add(worldTerrain.getLandscape());
    }
    
    public void initPlayer() {
    	
    	player = new Player(2f, 100, 200, settings, assetManager, guiNode);
    	
    	//Quirk where origin doesn't work
        Vector3f startPoint = worldTerrain.findGroundLevel(new Vector3f(1f, 0f, 1f));
        
        startPoint.addLocal(0, player.getHeight(), 0);
        
        player.getPhysControl().setPhysicsLocation(startPoint);
        
        //Logger.getLogger(WorldTerrain.class.getName()).log(Level.INFO, "Position in Physics\n", physControl.getPhysicsLocation());
    	bulletAppState.getPhysicsSpace().add(player.getPhysControl());
    }
    
    public void initAI(){
    	AIM.createMob(worldTerrain.getRandomStart().add(0,2,0), 100, 4);
    	AIM.createMob(worldTerrain.getRandomStart().add(0,2,0), 50, 2);
    	
    	Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    	mat.setColor("Color",ColorRGBA.Red);   
    	
    	for (Mob m: AIM.mobList()){
    		if (m != null){
    			for(Enemy e: m.getAll()){
    				if (e != null){
    					logger.log(Level.INFO, "Drew a guy at: {0}", e.getLocation());
    					Geometry enemy = ShapeGen.cubeGen("Enemy", mat, new Vector3f (2,2,2), e.getLocation(), e.getPhysControlEnemy());
    					enemy.setUserData("usable",false);
    					rootNode.attachChild(enemy);
    					bulletAppState.getPhysicsSpace().add(e.getPhysControlEnemy());
    				}
    			}
    		}
    	}
    }
    
    public void initBase() {
    	
    	Vector3f baseSize = new Vector3f(20, 10, 20);
    	
    	base = new PlayerBase(500, "Base", new Box(baseSize.x, baseSize.y, baseSize.z) );
    	Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    	mat.setColor("Color", ColorRGBA.DarkGray);
    	
    	base.setUserData("usable", true);
    
    	base.setMaterial(mat);
    	
    	base.setLocalTranslation(worldTerrain.findGroundLevel(new Vector3f(2,0,40)).addLocal(0, baseSize.y, 0));
    	rootNode.attachChild(base);
    }
    
    public void initAudio() {
    	audioManager = new AudioManager();
    	
    	AudioNode bowSound = audioManager.addAudio(assetManager, "Sounds/Weapons/bow.ogg", "bowFire", 2, false);
    	rootNode.attachChild(bowSound);
    	
    	AudioNode damageSound = audioManager.addAudio(assetManager, "Sounds/Damage/damage.ogg", "damage", 5, false);
    	rootNode.attachChild(damageSound);
    }
    
    
    public void initScreenText() {
    	textDisp = new PopupQueue(this, 3);
    	
		NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager,
				inputManager, audioRenderer, guiViewPort);
		
		nifty = niftyDisplay.getNifty();
		textDisp.createScreen(nifty);
		
		// attach the Nifty display to the gui view port as a processor
		guiViewPort.addProcessor(niftyDisplay);
		
		nifty.gotoScreen("hud");

		textDisp.queueText("Feel free to store your extra oxygen in your base");
		textDisp.queueText("You may want to start collecting some from the containers around you");
		textDisp.queueText("Welcome to OxyFPS, you may notice you are running out of Oxygen");
    }
        
    public void genAirCan() {
		
		Vector3f cubeStart = worldTerrain.getRandomStart();
		
		//Generate a cube
		Vector3f groundLevel = worldTerrain.findGroundLevel(cubeStart);
		
		//Tell AI about it
		AIM.addOOI(groundLevel);
		
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", ColorRGBA.LightGray);
		
		Geometry cube = ShapeGen.cubeGen("OxyTank", mat, new Vector3f(3,7,3), groundLevel);
		
		cube.setUserData("usable", true);
		cube.setUserData("aircapacity", 50f);
		
        rootNode.attachChild(cube);
        
        logger.log(Level.INFO, "Aircapacity: {0}", cube.getUserData("aircapacity"));
        bulletAppState.getPhysicsSpace().add(cube);
    }
    
    public BulletAppState getPhysState() {
    	return bulletAppState;
    }
}
