package Oxyfps;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.app.SimpleApplication;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;

public class InputController implements ActionListener, AnalogListener {

    private InputManager inputManager;
    private Camera cam;
    private Vector3f initialUpVec;
    
	private boolean fire;
	private boolean isLeft;
	private boolean isRight;
	private boolean isUp;
	private boolean isDown;

	private boolean jump;
	private boolean sprintP = false;

	private boolean wireframe;
	private boolean flyMode;

	private boolean isUsing;
	private float rotationSpeed;
	
	private int leftKey;
	private int rightKey;
	private int upKey;
	private int downKey;
	
	private int jumpKey;
	private int useKey;
	private int sprintKey;
	
	
	public InputController(InputManager inputManager, Camera cam) {
		this.inputManager = inputManager;


		setupBindings();

		this.cam = cam;
		
		initialUpVec = cam.getUp().clone();
		this.rotationSpeed = 1f;
	}
	
    private void setupBindings() {
    	
    	//Mouse Axis
    	inputManager.addMapping("Look_Left", new MouseAxisTrigger(MouseInput.AXIS_X, true));
    	inputManager.addListener(this, "Look_Left");	
    	inputManager.addMapping("Look_Right", new MouseAxisTrigger(MouseInput.AXIS_X, false));
    	inputManager.addListener(this, "Look_Right");	
    	inputManager.addMapping("Look_Up", new MouseAxisTrigger(MouseInput.AXIS_Y, true));
    	inputManager.addListener(this, "Look_Up");	
    	inputManager.addMapping("Look_Down", new MouseAxisTrigger(MouseInput.AXIS_Y, false));
    	inputManager.addListener(this, "Look_Down");	
    	    	
    	//Mouse
    	inputManager.addMapping("fire", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
    	inputManager.addListener(this, "fire");
    	
    	//Keyboard

		inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
		inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
		inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
		inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));


		inputManager.addMapping("Left", new KeyTrigger(leftKey));
		inputManager.addMapping("Right", new KeyTrigger(rightKey));
		inputManager.addMapping("Up", new KeyTrigger(upKey));
		inputManager.addMapping("Down", new KeyTrigger(downKey));
		
		inputManager.addListener(this, "Left");
		inputManager.addListener(this, "Right");
		inputManager.addListener(this, "Up");
		inputManager.addListener(this, "Down");


		inputManager.addMapping("use", new KeyTrigger(KeyInput.KEY_E));
		inputManager.addListener(this, "use");
		inputManager.addMapping("jump", new KeyTrigger(KeyInput.KEY_SPACE));
		inputManager.addMapping("use", new KeyTrigger(useKey));
		inputManager.addListener(this, "use");

		inputManager.addMapping("jump", new KeyTrigger(jumpKey));

		inputManager.addListener(this, "jump");
		inputManager.addMapping("sprint", new KeyTrigger(sprintKey));
		inputManager.addListener(this, "sprint");

		// Debug
		inputManager.addMapping("wireframe", new KeyTrigger(KeyInput.KEY_T));
		inputManager.addListener(this, "wireframe");
		inputManager.addMapping("flyMode", new KeyTrigger(KeyInput.KEY_P));
		inputManager.addListener(this, "flyMode");
		
	}

	/**
	 * Value is an isPressed method
	 */
	public void onAction(String binding, boolean value, float tpf) {

		if (binding.equals("fire")) {
			fire = value;
			// TODO: Rewrite this to hell
		} else if (binding.equals("Left")) {
			isLeft = value;
		} else if (binding.equals("Right")) {
			isRight = value;
		} else if (binding.equals("Up")) {
			isUp = value;
		} else if (binding.equals("Down")) {
			isDown = value;
		} else if (binding.equals("jump")) {
			jump = value;
		} else if (binding.equals("sprint")) {
			sprintP = value;
		} else if (binding.equals("use")) {
			isUsing = value;
		} else if (binding.equals("wireframe") && !value) {
			// Toggle binding on key down
			wireframe = !wireframe;
		} else if (binding.equals("flyMode") && !value) {
			flyMode = !flyMode;
			Logger.getLogger(InputController.class.getName()).log(Level.INFO,
					"FlyMode: {0}", flyMode);
		}
	}
	
	public void onAnalog(String binding, float value, float tpf) {

		Vector3f left = cam.getLeft();
		
		if (binding.equals("Look_Left")) {
			rotateCamera(value, initialUpVec);
		} else if (binding.equals("Look_Right")) {
			rotateCamera(-value, initialUpVec);
		} else if (binding.equals("Look_Up")) {
			rotateCamera(value, left);
		} else if (binding.equals("Look_Down")) {
			rotateCamera(-value, left);
		}
	}
	
	/**
	 * Rotate the camera 
	 * @param value How much to rotate
	 * @param axis Apply rotation on what axis
	 */
	protected void rotateCamera(float value, Vector3f axis){

        Matrix3f matrix = new Matrix3f();
        matrix.fromAngleNormalAxis(rotationSpeed * value, axis);

        Vector3f up = cam.getUp();
        Vector3f left = cam.getLeft();
        Vector3f dir = cam.getDirection();

        matrix.mult(up, up);
        matrix.mult(left, left);
        matrix.mult(dir, dir);

        Quaternion q = new Quaternion();
        q.fromAxes(left, up, dir);
        q.normalizeLocal();

        cam.setAxes(q);
    }
	
	public boolean isFire() {
		return fire;
	}

	public boolean isLeft() {
		return isLeft;
	}

	public boolean isRight() {
		return isRight;
	}

	public boolean isUp() {
		return isUp;
	}

	public boolean isDown() {
		return isDown;
	}

	public boolean hasJump() {
		return jump;
	}

	public boolean isUsing() {
		return isUsing;
	}

	//What is this doing here?
	public boolean isWireframe() {
		return wireframe;
	}

	public boolean canFly() {
		return flyMode;
	}

	public boolean isSprintPressed() {
		return sprintP;
	}

	public void setupKeyBindings(int up, int down, int left, int right,
			int use, int jump, int sprint) {
		this.leftKey = left;
		this.rightKey = right;
		this.upKey = up;
		this.downKey = down;
		this.jumpKey = jump;
		this.useKey = use;
		this.sprintKey = sprint;
		
		setupBindings();
	}
}
