package Oxyfps;

import java.util.HashMap;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioNode.Status;
import com.jme3.math.Vector3f;

public class AudioManager {

	private HashMap<String, AudioNode> audioFiles;
	
	public AudioManager() {
		audioFiles = new HashMap<String, AudioNode>(5);
		
	}
	
	public void addAudio(AudioNode audio) {
		
		audioFiles.put(audio.getName(), audio);
	}
	
	/**
	 * Add audio to manager and return for adding to
	 * root node.
	 * 
	 * @param assetManager 
	 * @param fileName Where is the file
	 * @param name Friendly name for finding audio in list
	 * @param volume How loud is the file
	 * @param loop Should the audio loop
	 * @return The AudioNode added to the manager
	 */
	public AudioNode addAudio(AssetManager assetManager, String fileName, String name, int volume, boolean loop) {
		//Create a buffered node from file
		AudioNode audio = new AudioNode(assetManager, fileName, false);
		
		audio.setName(name);
	    audio.setLooping(loop);
	    
	    //Create an ambient sound
	    audio.setPositional(false);
	    audio.setVolume(volume);
	    
	    audioFiles.put(name, audio);
	    return audio;
	}
	
	public AudioNode getAudio(String name) {
		return audioFiles.get(name);
	}
	
	//Toggle play/pause
	public void togglePlaySong(String name) {
		
		AudioNode audio = audioFiles.get(name);
		
		//If not looping, don't care
		if (audio.isLooping()) {
			if (audio.getStatus().name().equals(Status.Stopped)) {
				audio.play();
			} else {
				audio.stop();
			}
		}
	}
	
	public void playAudio(String name, boolean playLoop) {
		AudioNode audio = audioFiles.get(name);
		
		if (!playLoop) {
			audio.playInstance();
		} else {
			audio.play();
		}
	}
}
