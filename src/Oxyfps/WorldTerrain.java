package Oxyfps;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.geomipmap.lodcalc.DistanceLodCalculator;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.HillHeightMap;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;

/**
 * Stores all the data needed for generating and saving a world.
 * 
 * @author homelinen
 * 
 */
public class WorldTerrain {

	private long seed;
	private int size;
	private int iterations;
	private float minRad;
	private float maxRad;
	private int patchSize;
	
	private Random rand;

	private Material mat_terrain;

	private TerrainQuad terrain;
	private AbstractHeightMap heightMap;
	private RigidBodyControl landscape;

	public WorldTerrain(AssetManager assetManager) {

		// Create the seed (For storage later)
		Random randSeed = new Random();
		seed = randSeed.nextLong();

		size = 1025;
		iterations = 100;
		minRad = 200;
		maxRad = 400;

		heightMap = null;

		genTerrain();
		
		patchSize = 65;
		String worldName = "terrain";
		terrain = new TerrainQuad(worldName, patchSize, size, heightMap
				.getHeightMap());

		genTerrainGraphics(assetManager);

		genTerrainPhysics();
		
		//Use seed to re-crete positions
		this.rand = new Random(seed);
	}

	/**
	 * Generates the geometry using a Hill Algorithm
	 */
	private void genTerrain() {

		try {
			heightMap = new HillHeightMap(size, iterations, minRad, maxRad,
					seed);
			heightMap.flatten((byte) 0.7);
			heightMap.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the graphics elements (Probably should allow terrain to be passed
	 * 
	 * @param assetManager
	 */
	private void genTerrainGraphics(AssetManager assetManager) {

		mat_terrain = new Material(assetManager,
				"Common/MatDefs/Terrain/Terrain.j3md");
		mat_terrain.setTexture("Alpha", assetManager
				.loadTexture("Textures/Terrain/alphamap-new.png"));
		mat_terrain.setBoolean("useTriPlanarMapping", false);

		Texture grass = assetManager.loadTexture("Textures/Terrain/sand.jpg");
		grass.setWrap(WrapMode.Repeat);
		mat_terrain.setTexture("m_Tex1", grass);
		mat_terrain.setFloat("m_Tex1Scale", 256F);

		Texture dirt = assetManager.loadTexture("Textures/Terrain/cracked-sand.jpg");
		dirt.setWrap(WrapMode.Repeat);
		mat_terrain.setTexture("m_Tex2", dirt);
		mat_terrain.setFloat("m_Tex2Scale", 512F);
		
		Texture sheetRock = assetManager.loadTexture("Textures/Terrain/rock01.jpg");
		dirt.setWrap(WrapMode.Repeat);
		mat_terrain.setTexture("m_Tex3", sheetRock);
		mat_terrain.setFloat("m_Tex3Scale", 128F);

		terrain.setMaterial(mat_terrain);
		terrain.setLocalTranslation(0, 0, 0);

		// What does this do?
		terrain.setLocalScale(1.2f, 0.3f, 1.2f);
	}

	public void setupLOD(Camera playerCam) {
		TerrainLodControl control = new TerrainLodControl(this.terrain,
				playerCam);

		// TODO: Find optimal multiplier
		float multiplier = 2.7f;
		control.setLodCalculator(new DistanceLodCalculator(patchSize,
				multiplier));
		terrain.addControl(control);
	}

	/**
	 * Generate the Physics Body for terrain
	 * 
	 * @return
	 */
	private void genTerrainPhysics() {
		// Physics for terrain
		// TODO use a better collision model
		CollisionShape terrainShape = CollisionShapeFactory
				.createDynamicMeshShape(terrain);
		landscape = new RigidBodyControl(terrainShape, 0);
	}
    
	/**
	 * Return the ground level at the co-ordinate using a raycast
	 * 
	 * BUG: If x or z are zero, will fail
	 * 
	 * @param loc
	 *            Where to do the height check
	 * @return
	 */
	public Vector3f findGroundLevel(Vector3f loc) {

		Vector3f rayStart = loc.clone();
		rayStart.setY(-1000);

		// Create an upward facing ray
		Ray ray = new Ray(rayStart, new Vector3f(0, 1f, 0).normalizeLocal());
		CollisionResults results = new CollisionResults();

		// Check if ground and ray collide, store in results
		Node terrain = getTerrain().clone(); 

		terrain.collideWith(ray, results);
		
		Logger.getLogger(WorldTerrain.class.getName()).log(Level.INFO,
				"Ray: {0}\nTerrain Pos: {1}\n Results: {2}",
				new Object[]{ ray, getTerrain().getLocalTranslation(), results.size() });
		
		if (results.size() > 0) {
			float yVal = rayStart.y + results.getClosestCollision().getDistance();
	
			Logger.getLogger(WorldTerrain.class.getName()).log(Level.INFO,
					"Distance: {0}", yVal);
			return loc.setY(yVal);
		}
		
		return loc.add(0, 0, 0);
	}

	/**
	 * Find a random spot on the terrain
	 * @return
	 */
	public Vector3f getRandomStart() {
		
		Random rand = new Random();
		
		float x = rand.nextInt(size/2);
		float y = rand.nextInt(size/2);
		
		x = x * getRandPolar();
		y = y * getRandPolar();
		Vector3f startPos = new Vector3f(
		x,
		//y position will normally be overridden by findGroundStart
		0,
		y);
		
		Logger.getLogger(WorldTerrain.class.getName()).log(Level.INFO,
				"Random Start Pos: {0}", startPos);
		return startPos;
	}
	
	/**
	 * Return either 1 or -1 randomly
	 * @return
	 */
	public static int getRandPolar() {
	
		Random rand = new Random();
		
		if(rand.nextBoolean()) {
			return 1;
		} else {
			return -1;
		}
	}
	
	public TerrainQuad getTerrain() {
		return terrain;
	}

	public AbstractHeightMap getHeightMap() {
		return heightMap;
	}

	public RigidBodyControl getLandscape() {
		return landscape;
	}

	public Material getMatTerrain() {
		return mat_terrain;
	}

	public String getWorldName() {
		return terrain.getName();
	}
}
