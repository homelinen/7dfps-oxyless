/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Oxyfps.AI;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;

import Oxyfps.WorldTerrain;
import Oxyfps.entities.Enemy;
import Oxyfps.entities.Mob;

/**
 * 
 * @author Ryan
 *
 * Notes:
 * Use singleton, all enemies will use the same AI engine. Group attacks?
 * How do they breathe for so long? Reaver-like creatures! AHA! Terraforming
 * of the planet went tits-up and the company that scheduled this to happen
 * sent/hired/engineered savages to destroy every life form on the planet.
 * BOOM.
 */
public class AIManager {

	private List<Mob> MobList;
	private List<Vector3f> objOfInterest;
	private int mobsPresent;
	private int attackDistance;
	private WorldTerrain worldTerrain;

	private AIManager() {
		MobList = new ArrayList<Mob>();
		objOfInterest = new ArrayList<Vector3f>();
		mobsPresent = 0;
	}
	
	public static class AIManagerHolder{
		public static final AIManager AIMinstance = new AIManager();
	}
	
	public void setWT(WorldTerrain wt){
		worldTerrain = wt;
	}

	public void createMob(Vector3f loc, int health, int size) {
		MobList.add(new Mob(loc, health, size));
		mobsPresent++;
	}
	
	public void mergeMobs(Mob A, Mob B){
		if(A.equals(B) || A == null || B == null) return;
		for(Enemy E: B.getAll()){
			A.add(E);
		}
		this.removeMob(B.getID());
		mobsPresent--;
	}
	
	public void removeMob(int ID){
		if (ID < MobList.size()){
			MobList.set(ID,null);
			Logger.getLogger(Mob.class.getName()).log(Level.INFO,
					"Killed");
		}
	}

	public void addOOI(Vector3f v){
		objOfInterest.add(v);
	}

	
//	public void kill(int MobID, int enemyID){
//		MobList.get(MobID).kill(enemyID);
//	}
	

	public int getNextMobID() {
		int size = MobList.size();
		if (size == 0) return 0;
		for (int i = 0; i < size-1; i++) {
			if (MobList.get(i) == null) {
				return i;
			}
		}
		MobList.add(null);
		Logger.getLogger(Mob.class.getName()).log(Level.INFO,
				"Add a null");
		return size + 1;
	}

	public Vector3f findNearest(Enemy e, Vector3f prev) {
		Vector3f next = objOfInterest.get(0);
		float distance = e.getLocation().distance(next);
		float vDistance;
		int negation = 1;
		if (prev.getX()%2 == 0) negation = -negation;
		Vector3f newLocation = new Vector3f(worldTerrain.findGroundLevel(
				new Vector3f(prev.addLocal(2*negation, 2, 2*negation))));
		this.addOOI(newLocation);
		for (Vector3f v: objOfInterest){
			if (!v.equals(prev)){
				vDistance = e.getLocation().distance(v);
				if (vDistance < distance){
					next = v;
					distance = vDistance;
				}
			}
		}
		return next;
	}
	
	public Vector3f getWaypoint() {
		return worldTerrain.getRandomStart();
	}
	
	public static AIManager getInstance(){
		return AIManagerHolder.AIMinstance;
	}
	
	public List<Mob> mobList(){
		return MobList;
	}
	
	public void update(){
		for(Mob m: MobList){
			if (m != null){
				m.update();
			}
		}
	}
	
}
