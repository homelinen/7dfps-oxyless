package Oxyfps;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import Oxyfps.AI.AIManager;
import Oxyfps.entities.Enemy;
import Oxyfps.entities.Mob;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;

/**
 * Manage projectiles and bullets
 * 
 * @author homelinen
 * 
 */
public class ProjectileManager {

	private Node localRootNode;
	private AssetManager assetMan;
	private BulletAppState physState;
	
	public ProjectileManager(SimpleApplication app, BulletAppState physState) {
		this.localRootNode = app.getRootNode();
		this.assetMan = app.getAssetManager();
		this.physState = physState;
	}

	public void update() {
		List<Mob> mobs = AIManager.getInstance().mobList();
		
		for (Mob mob: mobs) {
			if (mob != null) {
				for (Enemy e: mob.getAll()) {
					if (e != null && e.isShooting()) {
						Vector3f fireDirection = e.getPhysControlEnemy().getViewDirection().clone();
						
						//Add to adjust for arc
						fireDirection.addLocal(0, 3, 0);
						Geometry bullet = fireProjectile(assetMan, e.getLocation(), fireDirection.normalize(), 200, 5);
						localRootNode.attachChild(bullet);
						physState.getPhysicsSpace().add(bullet.getControl(0));
					}
				}
			}
		}
	}
	
	
	/**
	 * Return a projectile with a given velocity
	 * 
	 * TODO: Material, and shape should be held in Type
	 */
	public static Geometry fireProjectile(AssetManager assetManager,
			Vector3f position, Vector3f direction, float speedFactor, float damage) {

		float bullRadius = 0.2f;
		// Graphical
		Sphere sphere = new Sphere(6, 6, bullRadius, true, true);
		Material mat = new Material(assetManager,
				"Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", ColorRGBA.Red);

		Geometry bullet = getProjectile("bullet", mat, sphere, position, damage);

		// Physical
		float bMass = 0.15f;

		RigidBodyControl bullBody = createPhysicsShape(bullet, bMass,
				direction, speedFactor);
		Logger.getLogger(ProjectileManager.class.getName()).log(Level.INFO,
				"Bullet Position: {0}", position);

		bullet.addControl(bullBody);

		return bullet;
	}

	/**
	 * Get the Geometry for a bullet at translated location
	 * 
	 * @param position
	 * @param damage: How much pain does the projectile cause
	 */
	private static Geometry getProjectile(String name, Material mat,
			Mesh shape, Vector3f position, float damage) {

		Geometry geom = new Geometry(name, shape);
		geom.setMaterial(mat);
		geom.setLocalTranslation(position);
		
		//Store damage factor in Geom
		geom.setUserData("damage", damage);
		return geom;
	}

	/**
	 * 
	 * @param shape
	 *            - The spatial that the physics body should represent
	 * @param mass
	 *            - The mass of the physics body
	 * @param direction
	 *            - Should be a normalised direction
	 * @param speedFactor
	 */
	private static RigidBodyControl createPhysicsShape(Spatial shape,
			float mass, Vector3f direction, float speedFactor) {

		RigidBodyControl body;

		// This sacrifices CPU time for one type of shape Creation
		CollisionShape physShape = CollisionShapeFactory
				.createDynamicMeshShape(shape);
		body = new RigidBodyControl(physShape, mass);

		body.setLinearVelocity(direction.mult(speedFactor));

		return body;
	}
}
