package Oxyfps;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 * Monitors projectiles and whether they should be destroyed or not
 * 
 * @author homelinen
 * 
 */
public class ProjectileGarbageCollector implements PhysicsCollisionListener {

	String worldName;
	Node rootNode;
	BulletAppState physState;

	public ProjectileGarbageCollector(String worldName, Node rootNode,
			BulletAppState physState) {
		this.worldName = worldName;
		this.rootNode = rootNode;
		this.physState = physState;
	}

	// TODO: Should be using collision groups
	public void collision(PhysicsCollisionEvent event) {
		Spatial deletedNode = null;

		// Check for collision against world
		if (event.getNodeA() != null
				&& event.getNodeA().getName().equals("bullet")) {

			deletedNode = event.getNodeA();

			physState.getPhysicsSpace().removeCollisionObject(
					event.getObjectA());
			rootNode.detachChild(event.getNodeA());
		} else if (event.getNodeB() != null
				&& event.getNodeB().getName().equals("bullet")) {

			deletedNode = event.getNodeB();

			physState.getPhysicsSpace().removeCollisionObject(
					event.getObjectB());
			rootNode.detachChild(event.getNodeB());
		}

		if (deletedNode != null) {
			Logger.getLogger(ProjectileGarbageCollector.class.getName()).log(
					Level.INFO, "Node {0} collides with world",
					deletedNode.getName());
		}
	}
}
