package Oxyfps.Util;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.text.StyleConstants.CharacterConstants;

import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.control.Control;
import com.jme3.scene.shape.Box;
import com.sun.corba.se.spi.ior.MakeImmutable;

public class ShapeGen {

	private ShapeGen() {
	}

	/**
	 * Create a cube and add it to the map
	 * 
	 * Cube and physics box are slightly miss-aligned
	 */
	public static Geometry cubeGen(Material mat, Vector3f size,
			Vector3f startPos) {

		return cubeGen("Box", mat, size, startPos);
	}
	
	
	public static Geometry cubeGen(String name, Material mat, Vector3f size, Vector3f startPos, CharacterControl charBody) {
		
	  	//Add the height of the cube to the ground level
    	startPos.addLocal(0, size.y, 0); 
    	
    	//Set box shape
        Box b = new Box(size.divide(2), size.x, size.y, size.z);
        Geometry geom = new Geometry(name, b);

        geom.setMaterial(mat);
        
        //Set the position of the box
        geom.setLocalTranslation(startPos);
        charBody.setPhysicsLocation(startPos);
        
        geom.addControl(charBody);
        Logger.getLogger(ShapeGen.class.getName()).log(Level.INFO, "Position is {1}, should be: {0}", 
        		new Object[] {startPos, charBody.getPhysicsLocation() });
 
        return geom;
	}
	
    /**
     * Create a cube and add it to the map
     * 
     * Cube and physics box are slightly miss-aligned
     */
    public static Geometry cubeGen(String name, Material mat, Vector3f size, Vector3f startPos) {
    	
    	//Add the height of the cube to the ground level
    	startPos.addLocal(0, size.y, 0); 
    	
    	//Set box shape
        Box b = new Box(size.divide(2), size.x, size.y, size.z);
        Geometry geom = new Geometry(name, b);

        geom.setMaterial(mat);
        
        //Set the position of the box
        geom.setLocalTranslation(startPos);
        
        RigidBodyControl body = makePhysBody(size, startPos);
        
        geom.addControl(body);
        Logger.getLogger(ShapeGen.class.getName()).log(Level.INFO, "Position is {1}, should be: {0}", 
        		new Object[] {startPos, body.getPhysicsLocation() });
 
        return geom;
    }

    private static RigidBodyControl makePhysBody(Vector3f size, Vector3f startPos) {
        
        //Create the physics body for the box
        BoxCollisionShape boxShape = new BoxCollisionShape(size);
        RigidBodyControl boxBody = new RigidBodyControl(boxShape, 4f);
        
        boxBody.setPhysicsLocation(startPos);
        
        return boxBody;
    }
}
