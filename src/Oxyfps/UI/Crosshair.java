package Oxyfps.UI;

import com.jme3.asset.AssetManager;
import com.jme3.ui.Picture;

/* Class to represent a crosshair */

public class Crosshair {

	private Picture pic;
	private static final int HEIGHT = 30;
	private static final int WIDTH = 30;

	// x and y are the center position
	public Crosshair(float x, float y, AssetManager mgr, String name) {
		pic = new Picture("name");
		pic.setImage(mgr, name, true);
		pic.setWidth(HEIGHT);
		pic.setHeight(WIDTH);
		pic.setPosition(x - WIDTH / 2, y - HEIGHT / 2);
	}

	public void update(float x, float y) {
		pic.setPosition(x - WIDTH / 2, y - HEIGHT / 2);
	}

	public Picture getGUIElement() {
		return this.pic;
	}

}
