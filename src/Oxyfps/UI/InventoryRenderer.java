package Oxyfps.UI;

import java.util.List;

import Oxyfps.entities.Item;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;

public class InventoryRenderer {
	private float xPos, yPos;
	private BitmapText invText;

	public InventoryRenderer(float x, float y, BitmapFont f) {
		xPos = x;
		yPos = y;
		invText = new BitmapText(f, false);
		invText.setSize(f.getCharSet().getRenderedSize());
		invText.setColor(ColorRGBA.White);
		invText.setText("Inventory");
		invText.setLocalTranslation(xPos, yPos, 0);
	}

	public BitmapText getGUIElement() {
		return invText;
	}

	public void update(List<Item> items) {
		invText.setText(getInvString(items));
	}

	private String getInvString(List<Item> items) {
		StringBuilder s = new StringBuilder("Inventory");
		for (Item i : items) {
			s.append("\n" + i.getName() + ":" + i.getQuantity());
		}

		return s.toString();
	}

}
