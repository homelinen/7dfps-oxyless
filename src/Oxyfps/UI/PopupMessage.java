package Oxyfps.UI;

public class PopupMessage implements Comparable<PopupMessage> {

	private String message;
	private float timePosted;
	
	public PopupMessage(String message, float time) {
		this.message = message;
		this.timePosted = time;
	}
	
	public int compareTo(PopupMessage message) {
		
		int result;
		if (timePosted > message.getTime()) {
			//Higher time is newer, so lower
			result = -1;
		} else if (timePosted < message.getTime()) {
			result = 1;
		} else {
			result = 0;
		}
		
		return result;
	}
	
	public String getMessage() {
		return message;
	}
	
	public float getTime() {
		return timePosted;
	}

}
