package Oxyfps.UI;

import com.jme3.asset.AssetManager;
import com.jme3.ui.Picture;

/* class to represent a simple status bar */

public class InfoBar {

	private float xPos, yPos, maxValue, minValue, currentValue;
	private Picture pic;

	public InfoBar(float x, float y, float max, float min, AssetManager mgr,
			String name) {
		xPos = x;
		yPos = y;
		maxValue = max;
		minValue = min;
		currentValue = max;

		pic = new Picture("name");
		pic.setImage(mgr, name, true);
		pic.setWidth(10);
		pic.setHeight(maxValue);
		pic.setPosition(xPos, yPos);
	}

	// value is a new value not a difference
	public void updateInfo(float value) {
		if (value >= maxValue) {
			currentValue = maxValue;
		} else if (value <= minValue) {
			currentValue = minValue;
		} else
			currentValue = value;

		updatePicture();
	}

	private void updatePicture() {
		pic.setHeight(currentValue);
	}

	public Picture getGUIElement() {
		return pic;
	}

}
