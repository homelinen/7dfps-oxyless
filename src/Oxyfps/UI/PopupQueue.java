package Oxyfps.UI;

import java.util.PriorityQueue;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.system.Timer;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.TextBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class PopupQueue extends AbstractAppState implements ScreenController {

	private ViewPort viewPort;
	private Node rootNode;
	private Node guiNode;
	private Node localRootNode = new Node("Start Screen RootNode");
	private Node localGuiNode = new Node("Start Screen GuiNode");
	private final ColorRGBA backgroundColor = ColorRGBA.Gray;
	
	private Nifty nifty;
	private Timer timer;

	private PriorityQueue<PopupMessage> textQueue;
	private long showedText;
	private long displayLength;
	
	boolean lastText;
	
	/**
	 * 
	 * @param app
	 * @param displayLength - How long to show messages in seconds
	 */
	public PopupQueue(SimpleApplication app, int displayLength) {
		this.rootNode = app.getRootNode();
		this.viewPort = app.getViewPort();
		this.guiNode = app.getGuiNode();
		timer = app.getTimer();
		
		textQueue = new PriorityQueue<PopupMessage>();
		showedText = timer.getTime();
		
		//Transform seconds to ticks
		this.displayLength = displayLength * timer.getResolution();
	}
	
	/**
	 * Used for when you need the Queue to be a Nifty Listener
	 */
	public PopupQueue() {	
	}

	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		/** init the screen */
	}

	@Override
	public void update(float tpf) {
		
		long timeDiff = timer.getTime() - showedText;
		/*
		 * Modify the shown text from queue
		 */
	 	if (timeDiff > displayLength && nifty != null) {
			// Nifty
			Element niftyElement = nifty.getCurrentScreen().findElementByName("info");
			if (niftyElement != null) {
				Logger.getLogger(PopupQueue.class.getName()).log(Level.INFO, "Nifty text: {0}", niftyElement.getRenderer(TextRenderer.class));
				
				if (niftyElement.getRenderer(TextRenderer.class) != null) {
					
					if ( textQueue.size() > 0 ) {
						niftyElement.getRenderer(TextRenderer.class).setText(textQueue.remove().getMessage());
						showedText = timer.getTime();
						
						lastText = false;
					} else {
						
						if (!lastText) {
							niftyElement.getRenderer(TextRenderer.class).setText("");
							lastText = true;
						}
					}
				}
			} else {
				Logger.getLogger(PopupQueue.class.getName()).log(Level.INFO, "No element by that name");
			}
    	}
	}

	@Override
	public void stateAttached(AppStateManager stateManager) {
		rootNode.attachChild(localRootNode);
		guiNode.attachChild(localGuiNode);
		viewPort.setBackgroundColor(backgroundColor);
	}

	@Override
	public void stateDetached(AppStateManager stateManager) {
		rootNode.detachChild(localRootNode);
		guiNode.detachChild(localGuiNode);
	}

	public void createScreen(Nifty nifty) {
		
		this.nifty = nifty;
		nifty.addScreen("hud", new ScreenBuilder("hud") {{
			controller(new PopupQueue());

			layer(new LayerBuilder("background") {{
				childLayoutVertical();
				
				// panel added
				panel(new PanelBuilder("panel_top") {{
					childLayoutVertical();
					alignCenter();
					height("50%");
					width("50%");

					// panel added
					panel(new PanelBuilder("padding-text-top") {{
						childLayoutCenter();
						alignCenter();
						height("75%");
						width("100%");
					}});
					
					// add text
	                text(new TextBuilder("info") {{
	                    text("");
	                    font("Interface/Fonts/Default.fnt");
	                    height("25%");
	                    width("100%");
	                }});
	                
				}});

			}});

		}}.build(nifty));
	}

	public void queueText(String update) {
		
		PopupMessage message = new PopupMessage(update, timer.getTimeInSeconds());
		textQueue.add(message);
	}
	
	public void bind(Nifty arg0, Screen arg1) {
		// TODO Auto-generated method stub
		
	}

	public void onEndScreen() {
		// TODO Auto-generated method stub
		
	}

	public void onStartScreen() {
		// TODO Auto-generated method stub
		
	}

}
