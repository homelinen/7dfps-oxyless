package Oxyfps.UI;

import Oxyfps.entities.Player;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;

/* Class to represent the basic UI layout - Magic number wise */

public class PlayerUI {
	private InfoBar healthBar;
	private InfoBar oxygenBar;
	private Crosshair crosshair;
	private InventoryRenderer inventoryRenderer;

	private Player player;

	public PlayerUI(Player player2, AppSettings settings, AssetManager mgr,
			Node gui) {

		player = player2;

		healthBar = new InfoBar(settings.getWidth() / 16,
				settings.getHeight() / 32, 200, 10, mgr,
				"Textures/Player/healthBar.jpg");
		oxygenBar = new InfoBar(settings.getWidth() / 16 + 20, settings
				.getHeight() / 32, 200, 10, mgr,
				"Textures/Player/oxygenBar.jpg");
		crosshair = new Crosshair(settings.getWidth() / 2,
				settings.getHeight() / 2, mgr, "Textures/Player/crosshair.png");
		inventoryRenderer = new InventoryRenderer(settings.getWidth() - 100,
				settings.getHeight() - 10, mgr
						.loadFont("Interface/Fonts/Default.fnt"));

		gui.attachChild(healthBar.getGUIElement());
		gui.attachChild(oxygenBar.getGUIElement());
		gui.attachChild(crosshair.getGUIElement());
		gui.attachChild(inventoryRenderer.getGUIElement());

	}

	public void update() {
		healthBar.updateInfo(player.getHealth());
		oxygenBar.updateInfo(player.getOxygen());
		inventoryRenderer.update(player.getInventory());
	}

}
