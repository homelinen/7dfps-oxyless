# OxyFPS #

This is the base for a [7DFPs](http://7dfps.org) entry.

## Authors ##

* Calum Gilchrist
* Ryan Wells
* Blair Archibald

## Ideas and Elements ##

You are on an airless planet with only a suit, a crossbow and a tank of air.

Your objective is to survive as long as you can while the corporate goons that
caused this terraforming catastrophe hunt you down.

## Currently ##

This is currently the 7DFPS release. All the code was written within the week
so it's very incomplete and very buggy. 

There is currently no story, but if we continue with the idea we could probably
create something interesting.

## Controls ##

Currently: 

* Movement: WASD
* Use: e
* Jump: Space
* Sprint: LShift

## Aim ##

Collect oxygen from cannisters and from the thugs you kill. Use this oxygen to 
continue playing and get enough oxygen gathered together to begin a new
terraforming project

## License ##

All code is licensed under the [Open Source MIT License](http://www.opensource.org/licenses/MIT) 

Assets listed in the Credits are subject to different licenses, but everything
not labelled as such is free to modify, sell and distribute.


